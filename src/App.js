import React, { Component } from 'react';
import Header from './components/Header';
import Intro from './components/Intro';
import About from './components/About';
import Why from './components/Why';
import Services from './components/Services';
import Work from './components/Work';
import Testimonials from './components/Testimonials';
import Contact from './components/Contact';
import Footer from './components/Footer';

const pages = ["Home", "About", "Services", "Our Work", "Testimonials", "Contact"];

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {

		}
	}

	render() {
		return (
			<div className="App">
				<Header menuItems={pages} />
				<Intro />
				<About />
				<Why />
				<Services />
				<Work />
				<Testimonials />
				<Contact />
				<Footer />
			</div>
		);
	}
}

export default App;
