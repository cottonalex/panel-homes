import React, { Component } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt, faPhone, faAt, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Col, FormGroup, Button,  FormControl, HelpBlock } from 'react-bootstrap';

const API_PATH = 'https://panelhomes.co.nz/api/contact/index.php';

const FieldGroup = ({ id, label, help, ...props }) => {
    return (
        <FormGroup controlId={id}>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    )
}

class Contact extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            name: '',
            email: '',
            phone: '',
            message: '',
            mailSent: false,
            error: null,
            isSecure: true,
            formSent: false,
            showSpinner: false
        };

        //this.formSent = false;
        //this.showSpinner = false;
    }

    handleFormSubmit = e => {
        e.preventDefault();
        const getState = JSON.stringify(this.state);
        //console.log(getState);
        this.setState({
            showSpinner: true,
        });

        axios({
            method: 'post',
            url: `${API_PATH}`,
            headers: { 'content_type': 'application/json' },
            data: getState
        }).then(result => {
            // console.log(result.data)
            // console.log(result.data.sent);
            // console.log(result.data['sent'])

            //const getResult = JSON.parse(result.data.sent)

            //console.log(getResult);

            // this.setState({
            //     mailSent: getResult,
            //     formSent: true
            // });

            // this.setState({
            //     showSpinner: false,
            //     formSent: true
            // });

            //this.formSent = true;
            //this.showSpinner = false;
        }).catch(error => {
            this.setState({
                error: error.message
            });
        });

        

        setTimeout(() => {
            this.setState({
                showSpinner: false,
                formSent: true
            });

            this.updateMapHeight();
        }, 5000)
    }

    inputValues = (e, input) => {
        this.setState({
            [input]: e.target.value
        });
    }

    updateMapHeight = () => {
        let getContactContentHeight = document.querySelector('.contact-content').offsetHeight;
        document.getElementById('map').style.height = `${getContactContentHeight}px`;
    }

    componentDidMount = () => {
        this.updateMapHeight();
        

        window.onresize = () => {
            //console.log('resizing');
            this.updateMapHeight();
        }
    }


    render() {
        return (
            <section className="contact">
                <Col xs={12} lg={6} className="no-padding">
                    <div id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d92591.47070446936!2d172.52903824510702!3d-43.51311012473127!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d322f4863c5ed01%3A0x500ef8684799945!2sChristchurch%2C+New+Zealand!5e0!3m2!1sen!2sau!4v1544014884899" frameBorder="0" allowFullScreen title="Christchurch"></iframe>
                    </div>
                </Col>
                <Col xs={12} lg={6} className="no-padding">
                    <div className="contact-content">
                        <h1>Contact Us</h1>
                        <p>Feel free to get in contact and we will be happy to provide a free quote on what is required to be done.</p>
                        <p><FontAwesomeIcon icon={faMapMarkerAlt} /> Christchurch, New Zealand</p>
                        <p><FontAwesomeIcon icon={faPhone} /> <a className="phone" href="tel:027 845 4232">027 845 4232 (Adam Cotton)</a></p>
                        <p><FontAwesomeIcon icon={faPhone} /> <a className="phone" href="tel:021 663 462">021 663 462 (Ben Strydom)</a></p>
                        <p><FontAwesomeIcon icon={faAt} /> <a href="mailto:info@panelhomes.co.nz">info@panelhomes.co.nz</a></p>
                        <form>
                            <FieldGroup
                                id="formControlsText"
                                type="text"
                                placeholder="Your Name"
                                onChange={(e) => this.inputValues(e, 'name')} />
                            <FieldGroup
                                id="formControlsEmail"
                                type="email"
                                placeholder="Your Email"
                                onChange={(e) => this.inputValues(e, 'email')} />
                            <FieldGroup
                                id="formControlsPhone"
                                type="text"
                                placeholder="Your Phone"
                                onChange={(e) => this.inputValues(e, 'phone')} />
                            <FormGroup controlId="formControlsTextarea">
                                <FormControl 
                                    componentClass="textarea" 
                                    placeholder="Message:"
                                    onChange={(e) => this.inputValues(e, 'message')} />
                            </FormGroup>

                            {!this.state.formSent ?
                                <Button 
                                    type="submit"
                                    onClick={(e) => {
                                        this.handleFormSubmit(e);
                                    }}>
                                    Submit 
                                    {this.state.showSpinner ? 
                                    <FontAwesomeIcon icon={faSpinner} /> 
                                    : null}
                                </Button>
                            :
                                <div className="sent">
                                    <span>Thank you for contacting us.<br />We will be sure to get back to you as fast as posible.</span>
                                </div>
                            }
                        </form>
                    </div>
                </Col>
            </section>
        )
    }
}

export default Contact;