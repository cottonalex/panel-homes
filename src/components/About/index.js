import React from 'react';

const About = () => {
    return (
        <section className="about">
            <div className="about-contents">
                <div className="content">
                    <div>
                        <h1>Who Are We?</h1> 
        
                        <p>Panel Homes was born after my business partner and I realised that we needed to provide people with stronger and safer homes. After all the earthquakes here in Christchurch we needed to change the way builders were building homes. By building walls with pure concrete you can trust your home will be safe from any future earthquakes.</p>

                        <p>As best friends working with each other for over 10 years, our combined experience will reassure you that we and our friendly team can build you your dream home or fix any issues you have in your current home. We are passionate about what we do and we love seeing the excitment on our customers faces after we hand them the keys to their new home.</p>
                    </div>
                </div>
                <div className="image-block"></div>
            </div>
        </section>
    );
}

export default About;