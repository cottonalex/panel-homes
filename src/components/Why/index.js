import React from 'react';

const Why = () => {
    return (
        <section className="why">
            <div className="why-contents">
                <div className="image-block"></div>
                <div className="content">
                    <div>
                        <h1>Why Choose Us?</h1> 
        
                        <p>We at Panel Homes use our years of experience in the construction industry to give you the best quality building, or any other construction needs you desire.</p>  
        
                        <p>We will personally work with you to achieve a high standard of design and ensure it is completed within your desired time frame.</p>
        
                        <p>We are licensed builder practitioners who devote our time to insure you that you will be happy with the work and outcome of the job you require. We are also qualified in making professional tilt slabs for your home or building.</p>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Why;