import React, { Component } from 'react';
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';
import Images from './Images';


class Work extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentImage: 0
        }
    }

    openLightbox = (event, obj) => {
        this.setState({
            currentImage: obj.index,
            lightboxIsOpen: true
        });
    }

    closeLightbox = () => {
        this.setState({
            currentImage: 0,
            lightboxIsOpen: false
        });
    }

    goToPrevious = () => {
        this.setState({
            currentImage: this.state.currentImage - 1
        });
    }
    
    goToNext = () => {
        this.setState({
           currentImage: this.state.currentImage + 1 
        });
    }

    render() {
        return (
            <section className="our-work">
                <div className="our-work-content text-center">
                    <h1>Our Work</h1>
                    <Gallery photos={Images} margin={5} onClick={this.openLightbox} onMouseEnter={() => alert()} />
                    <Lightbox 
                        images={Images}
                        backdropClosesModal={true}
                        onClose={this.closeLightbox}
                        onClickPrev={this.goToPrevious}
                        onClickNext={this.goToNext}
                        currentImage={this.state.currentImage}
                        isOpen={this.state.lightboxIsOpen} />           
                </div>
            </section>
        );
    }
}

export default Work;