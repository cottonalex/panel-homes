import img1 from './images/1.jpg';
import img2 from './images/2.jpg';
import img3 from './images/3.jpg';
import img4 from './images/4.jpg';
import img5 from './images/5.jpg';
import img6 from './images/6.jpg';
import img7 from './images/7.jpg';
import img8 from './images/8.jpg';
import img9 from './images/9.jpg';
import img10 from './images/10.jpg';
//import img11 from './images/11.jpg';
import img12 from './images/12.jpg';
import img13 from './images/13.jpg';
import img14 from './images/14.jpg';
import img15 from './images/15.jpg';
import img16 from './images/16.jpg';
import img17 from './images/17.jpg';


const Images = [
    { src: img1, width: 4, height: 5 },
    { src: img2, width: 2, height: 2 },
    { src: img3, width: 3, height: 4 },
    { src: img4, width: 3, height: 2 },
    { src: img5, width: 3, height: 2 },
    { src: img6, width: 4, height: 3 },
    { src: img7, width: 3, height: 3 },
    { src: img8, width: 4, height: 3 },
    { src: img9, width: 4, height: 5 },
    { src: img10, width: 4, height: 5 },
    // { src: img11, width: 2, height: 3 },
    { src: img12, width: 3, height: 4 },
    { src: img13, width: 3, height: 4 },
    { src: img14, width: 7, height: 4 },
    { src: img15, width: 4, height: 3 },
    { src: img16, width: 5, height: 4 },
    { src: img17, width: 6, height: 3 },
];

export default Images;