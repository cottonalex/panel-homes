import React from 'react';
import { Carousel } from 'react-bootstrap';
import TheCraneman from '../../images/the-craneman-logo.png';
import Monadelphous from '../../images/monadelphous-logo.png';

const Testimonials = () => {
    return (
        <section className="testimonials">
            <div className="testimonials-content text-center">
                <h1>Testimonials</h1>
                <p className="description">Just a few things our clients have said about us and our work.</p>
                <Carousel controls={false} interval={10000}>
                    <Carousel.Item>
                        <div className="item-content">
                            <h3 className="quote">"Adam and Ben were a massive help in completing our big project. They were fast, effecient and easy to communicate with. I would definitely recommend these guys to anyone looking for commercial builds."</h3>
                            <p className="quote-by">Liam - Monadelphous</p>
                            <img className="quote-logo" src={Monadelphous} alt="Monadelphous Logo" />
                        </div>
                    </Carousel.Item>
                    <Carousel.Item>
                        <div className="item-content">
                            <h3 className="quote">"You guys did an awesome job, it was exactly what I was after for my property and I couldn't be happier!"</h3>
                            <p className="quote-by">Mel Prinsloo - The Craneman Ltd</p>
                            <img className="quote-logo" src={TheCraneman} alt="The Craneman Logo" />
                        </div>
                    </Carousel.Item>
                </Carousel>
            </div>
        </section>
    )
}

export default Testimonials;