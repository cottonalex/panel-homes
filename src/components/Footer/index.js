import React from 'react';

const Footer = () => {
    return (
        <div className="footer">
            <div className="footer-content text-center">
                <p>&copy; 2019. All Rights Reserved. Panel Homes Construction. Website design and developed by <a href="https://www.alxdigital.com.au" target="_blank" rel="noopener noreferrer">ALX Digital</a>.</p>
            </div>
        </div>
    )
}

export default Footer;