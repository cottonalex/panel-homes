import React from 'react';
import img1 from '../../images/residential-construction.jpg';
import img2 from '../../images/foundation.jpg';
import img3 from '../../images/maintenance.jpg';

const serviceContent = [
    {
        bgImage: img1,
        title: "Residential Construction",
        text: "We will work with you to create the home you have always dreamt of at an affordable rate. Arrange a meeting with us and we can get started on designing a floor plan that satisfies all your needs."
    }, 
    {
        bgImage: img2,
        title: "Tilt Panels & foundation",
        text: "We are specialised in creating tilt slab panels which means we can meet any of your requirements. From small commercial shops to a tilt slab residential home, we can create something that you may have thought about but couldn't fulfil due to expense or time."
    }, 
    {
        bgImage: img3,
        title: "Property Maintenance",
        text: "Properties are a never ending cycle of work and bills, but we will make it easier for you to arrange what needs to be done to finish your home, as well as fixing old or broken items to make the property new again."
    }
]

const Services = () => {
    return (
        <section className="services">
            <div className="services-content text-center">
                <h1>Our Services</h1>
                <div className="services-blocks">
                {serviceContent.map((service, index) => {
                    return (
                        <div key={index} className="service-item">
                            <div className="bg-img" style={{background: `url(${service.bgImage}) no-repeat center/cover`}}></div>
                            <div className="service-item-content">
                                <h3>{service.title}</h3>
                                <p>{service.text}</p>
                            </div>
                        </div>
                    )
                })}
                </div>
            </div>
        </section>
    );
}

export default Services;