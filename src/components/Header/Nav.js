import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Breakpoint from 'react-responsive';
import { Col } from 'react-bootstrap';
import MenuItems from './MenuItems';


class Nav extends Component {
    constructor(props) {
        super(props);

        this.state = {
            menuActive: false
        }
    }

    toggleMenu = () => {
        this.setState({
            menuActive: !this.state.menuActive
        });
    }

    render() {
        const {menuActive} = this.state;
        return (
            <div className="nav text-center">
                <Breakpoint maxWidth={767}>
                    <div className="mobile-menu" onClick={() => this.toggleMenu()}>
                        <Col xs={6} className="no-padding text-left">
                            <span>Menu</span>
                        </Col>
                        <Col xs={6} className="no-padding text-right">
                            <div className={`menu hamburger hamburger--spin ${menuActive ? 'is-active' : null}`}>
                                <div className="hamburger-box">
                                    <div className="hamburger-inner"></div>
                                </div>
                            </div>
                        </Col>
                    </div>
                    {menuActive ? 
                    <MenuItems 
                        items={this.props.menuItems}
                        showArrows={true} />
                    : null}
                </Breakpoint>

                <Breakpoint minWidth={768}>
                    <div className="menu">
                        <MenuItems 
                            items={this.props.menuItems}
                            showArrows={false} />
                    </div>
                </Breakpoint>
            </div>
        );
    }
}

Nav.propTypes = {
    menuItems: PropTypes.array.isRequired
}

export default Nav;