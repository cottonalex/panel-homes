import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import scrollToElement from 'scroll-to-element';

const goToSection = menuItem => {
    let section = menuItem.toLowerCase().replace(' ', '-');
    
    if(section === 'home') {
        section = 'intro';
    }

    scrollToElement(`.${section}`, {
        offset: -85
    });

} 

const MenuItems = ({ items, showArrows }) => {
    return (
        <ul>
            {items.map((navItem, index) => {
                return (
                    <li key={index}>
                        <div 
                            className="nav-item"
                            onClick={() => goToSection(navItem)}>
                            <span>{navItem}</span>
                            {showArrows ?
                            <FontAwesomeIcon icon={faChevronRight} />
                            : null}
                        </div>
                    </li>
                ) 
            })}     
        </ul>
    );
}

MenuItems.propTypes ={
    items: PropTypes.array.isRequired,
    showArrows: PropTypes.bool.isRequired
}

export default MenuItems;