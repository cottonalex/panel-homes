import React from 'react';
import PropTypes from 'prop-types';
import Nav from './Nav';
import Logo from '../../images/logo.png';

const Header = ({ menuItems }) => {
    return (
        <div className="header">
            <a className="logo-wrapper center-block" href="/">
                <img className="logo" src={Logo} alt="logo" />
            </a>
            <Nav menuItems={menuItems} />
        </div>
    );
}

Header.propTypes = {
    menuItems: PropTypes.array.isRequired
}


export default Header;