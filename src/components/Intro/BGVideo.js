import React from 'react';
import bgVideo from '../../video/bg.mov';

const BGVideo = () => {
    return (
        <video autoPlay playsInline muted loop id="bg-video">
            <source src={bgVideo} />
        </video>
    );
}

export default BGVideo;