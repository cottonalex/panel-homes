import React from 'react';
import BGVideo from './BGVideo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import scrollToElement from 'scroll-to-element';

const scrollTo = element => {
    scrollToElement(`.${element}`, {
        offset: -90
    });
}

const Intro = () => {
    return (
        <div className="intro">
            <BGVideo />
            <div className="intro-content">
                <h1><span>Welcome To</span> Panel Homes</h1>
                <p>We at panel homes want to offer you any type of construction that you are after whether it be a brand new home build to as simple as building a new fence or pouring concrete slabs for spas or driveways.<br />Get a free quote to see how we can help you out.</p>
                <button 
                    className="get-quote"
                    onClick={() => scrollTo('contact')}>
                    Get a free quote
                </button>
            </div>
            <FontAwesomeIcon 
                icon={faChevronDown} 
                onClick={() => scrollTo('about')} />
        </div>
    );
}

export default Intro;