<?php
   /************** CHANGE TO INDEX.PHP FOR PRODUCTION IF LOADING VIA AJAX *************/
    header("Access-Control-Allow-Origin: *");
    $rest_json = file_get_contents("php://input");
    $_POST = json_decode($rest_json, true);

    $isSecure = $_POST['isSecure'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $message = $_POST['message'];

    var_dump($_POST);

    // Import PHPMailer classes into the global namespace
    // These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;  

    //Load composer's autoloader
    //require 'vendor/autoload.php';

    require 'phpmailer/src/PHPMailer.php';
    require 'phpmailer/src/SMTP.php';
    require 'phpmailer/src/Exception.php';

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions

    try {
        //Server settings
        
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.panelhomes.co.nz';                // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'info@panelhomes.co.nz';            // SMTP username
        $mail->Password = 'concretemungrel';                     // SMTP password
        $mail->SMTPSecure = 'tsl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($email, $name);
        $mail->addAddress('info@panelhomes.co.nz', 'Panel Homes');     // Add a recipient
        $mail->addReplyTo($email, $name);
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        //Attachments
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

        $msg = '<b>Name:</b> ' . $name . '<br />';
        $msg .= '<b>Email:</b> ' . $email . '<br />';
        $msg .= '<b>Phone:</b> ' . $phone . '<br />';
        $msg .= '<b>Message:</b> ' . $message;

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Panel Homes - Website Enquiry';
        $mail->Body    = $msg;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        // If isSecure is equal to true and exists then send email, otherwise return 404. To stop direct linking and sending emails on page load.
        if(isset($isSecure) && $isSecure == 'true') {
            $mail->send();
            //echo 'Message has been sent';

            echo json_encode(array(
                "sent" => true
            ));
        } else {
           header("HTTP/1.0 404 Not Found");
        }

    } catch (Exception $e) {
        //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;

        echo json_encode([
            "sent" => false,
            "message" => "Sorry something went wrong. Message could not be sent. Mailer Error:"
        ]);
    }

?>